var GofileAPI = function () {
  var inst = this;

  /*
   * Send files to Gofile API
   * Parameters: fileAsBlob
   */
  this.sendFile = function (fileAsBlob, fileName) {
    inst.makeRequest("https://apiv2.gofile.io/getServer", function (r) {
      let req = JSON.parse(r),
        serverID = req.data.server,
        convertToFile = new File([fileAsBlob], fileName, {
          type: "text/plain"
        }),
        formData = new FormData();
      formData.append("file", convertToFile);

      inst.makeRequest(
        "https://" + serverID + ".gofile.io/uploadFile",
        function (r) {
          let event = new CustomEvent("gf-api:upload-success", { detail: r });
          document.dispatchEvent(event);
        },
        formData,
        "POST"
      );
    });
  };

  /*
   * Check if a file exists on Gofile server
   * Parameters: uploadID, callback
   */
  this.existsFile = function (uploadID, callback) {
    inst.makeRequest(
      "https://apiv2.gofile.io/getServer?c=" + uploadID,
      function (r) {
        let req = JSON.parse(r);
        callback(req.status == "ok");
      }
    );
  };

  /*
   * Get file list from Gofile API
   * Parameters: uploadID, callback
   */
  this.getFilesLink = function (uploadID, callback) {
    inst.makeRequest(
      "https://apiv2.gofile.io/getServer?c=" + uploadID,
      function (r) {
        let req = JSON.parse(r);
        inst.makeRequest(
          "https://" + req.data.server + ".gofile.io/getUpload?c=" + uploadID,
          function (r) {
            let req = JSON.parse(r);
            if (req.status == "ok") {
              callback(req.data);
            } else {
              inst.logFailed("File is unavailable!");
            }
          }
        );
      }
    );
  };

  this.makeRequest = function (
    url,
    callback,
    reqContent = "",
    reqType = "GET"
  ) {
    let http = new XMLHttpRequest();
    http.open(reqType, url, true);

    http.onload = function () {
      if (http.status >= 200 && http.status < 400) {
        callback(http.response);
      } else {
        inst.logFailed("Server response: " + http.status);
      }
    };

    http.onerror = function (e) {
      inst.logFailed(e);
    };

    http.send(reqContent);
  };

  this.logFailed = function (e) {
    let event = new CustomEvent("gf-api:request-failed", { detail: e });
    document.dispatchEvent(event);
  };
};

/* GetSimpleForm API */
// TODO: Move GSF API to a new file

var getSimpleFormAPI = function (token, adminToken = "") {
  this.publicToken = token;
  this.adminToken = adminToken;
  var inst = this;

  /*
   * Send data to getSimpleForm API
   * Needs a publicToken
   * Parameters: content ( a dictionary of all form values )
   */
  this.sendEntry = function (content) {
    /* Extract data */
    let urlEncodedData = "",
      urlEncodedDataPairs = ["redirect_to=https://getsimpleform.com/"];

    for (let name in content) {
      urlEncodedDataPairs.push(
        encodeURIComponent(name) + "=" + encodeURIComponent(content[name])
      );
    }
    urlEncodedData = urlEncodedDataPairs.join("&").replace(/%20/g, "+");

    inst.makeRequest(
      "https://getsimpleform.com/messages?form_api_token=" + inst.publicToken,
      function (r) {
        let event = new CustomEvent("gsf-api:send-success", { detail: r });
        document.dispatchEvent(event);
      },
      urlEncodedData,
      "POST"
    );
  };

  /*
   * Get all entries in a getSimpleForm account
   * Needs an adminToken
   * Parameters: callback
   * TODO: This function needs testing (check if Content-type header is okay)
   */
  this.getEntries = function (callback) {
    inst.makeRequest(
      "https://getsimpleform.com/messages.json?api_token=" + inst.adminToken,
      function (r) {
        let req = JSON.parse(r);
        callback(req);
      }
    );
  };

  /*
   * Delete an entry from getSimpleForm
   * Needs an adminToken
   * Parameters: entryID
   * TODO: This function needs testing
   */
  this.deleteEntry = function (entryID) {
    let urlEncodedData = "_method=delete";
    inst.makeRequest(
      "https://getsimpleform.com/messages/" +
        entryID +
        "?api_token=" +
        adminToken,
      function (r) {
        let event = new CustomEvent("gsf-api:delete-success", { detail: r });
        document.dispatchEvent(event);
      },
      urlEncodedData,
      "POST"
    );
  };

  this.makeRequest = function (
    url,
    callback,
    reqContent = "",
    reqType = "GET"
  ) {
    let http = new XMLHttpRequest();
    http.open(reqType, url, true);

    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    http.onload = function () {
      if (http.status >= 200 && http.status < 400) {
        callback(http.response);
      } else {
        inst.logFailed("Server response: " + http.status);
      }
    };

    http.onerror = function (e) {
      inst.logFailed(e);
    };

    http.send(reqContent);
  };

  this.logFailed = function (e) {
    let event = new CustomEvent("gsf-api:request-failed", { detail: e });
    document.dispatchEvent(event);
  };
};
