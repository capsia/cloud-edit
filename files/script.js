var gsf;

/* Make JSON request */
function getSentData() {
  var privateToken = document.getElementById("privateToken").value;
  gsf = new getSimpleFormAPI("", privateToken);
  gsf.getEntries(function (r) {
    var sentResponses = [];
    for (let el of r) {
      el = el.data;
      if (el.serviceID == "capsiaCloudEdit") {
        sentResponses.push({
          fileID: el.fileID,
          docName: el.docName ? el.docName : "Untitled document"
        });
      }
    }
    displayUI(sentResponses);
  });
}

/* Generate UI content */
function displayUI(sentResponses) {
  /* Set QB Global Data */
  for (el of sentResponses) {
    var userDIV = document.createElement("article");
    var docName = document.createElement("h2");
    var docURL = document.createElement("a");
    var docDl = document.createElement("a");
    var docImg = document.createElement("img");
    var docDlImg = document.createElement("img");
    docImg.src = "img/mime-x-doc.svg";
    docDlImg.src = "img/download.svg";
    docName.innerHTML = el.docName;
    docURL.innerHTML = "https://cloudedit.gq/?gfid=" + el.fileID;
    docURL.href = "https://cloudedit.gq/?gfid=" + el.fileID;
    docURL.target = "_blank";
    docURL.setAttribute("class", "cceDocUrl");
    let gfID = el.fileID;
    docDl.onclick = function (e) {
      let gf = new GofileAPI();
      gf.getFilesLink(gfID, function (r) {
        window.open(r.files[Object.keys(r.files)[0]].link, "_blank");
      });
    };
    docName.onclick = function (e) {
      document.href = "https://cloudedit.gq/?gfid=" + el.fileID;
    };
    docDl.appendChild(docDlImg);
    userDIV.appendChild(docImg);
    userDIV.appendChild(docName);
    userDIV.appendChild(docDl);
    userDIV.appendChild(docURL);
    document.getElementById("content").appendChild(userDIV);
  }
}

/* Delete obsolete entries: W! This function is destructive */
function deleteObsoleted() {
  gsf.getEntries(function (r) {
    for (let el of r) {
      if (el.data.serviceID == "capsiaCloudEdit") {
        console.log(el);
        let gf = new GofileAPI();
        gf.existsFile(el.data.fileID, function (r) {
          if (!r) {
            console.log([
              el.id,
              el.data.docName,
              el.data.fileID,
              el.created_at
            ]);
            if (
              window.confirm(
                el.data.docName +
                  ", created at: " +
                  el.created_at +
                  " will be deleted. Are you sure?"
              )
            ) {
              gsf.deleteEntry(el.id);
            }
          }
        });
      }
    }
  });
}
