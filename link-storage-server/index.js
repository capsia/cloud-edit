// Importing express
let express = require("express"),
  bodyParser = require("body-parser");
cors = require("cors");

// Import routes
let apiRoutes = require("./routes");

// Initializing the app
let app = express();

// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

// Define server port
const PORT = process.env.port || 3000;

// Send message for default route
app.get("/", function (req, res) {
  res.send("Capsia connected services v0.1 : Gofile link storage service");
});

// Use Api routes in the App
app.use("/api", apiRoutes);

// Listen to specified port(always keep this at bottom of the file)
app.listen(PORT, function () {
  console.log("Server has started on port " + PORT);
});
