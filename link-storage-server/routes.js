// Import express router
let Router = require("express").Router();

// Set default API response
Router.get("/", function (req, res) {
  res.json({
    status: "WORKING",
    message:
      "Capsia connected services v0.1 : Gofile link storage service : Service is UP"
  });
});

// Import controller
var dataController = require("./dataController");

// Book routes
Router.route("/admin").get(dataController.index);

Router.route("/user")
  .get(function (req, res) {
    res.json({
      status: "WORKING",
      message:
        "Capsia connected services v0.1 : Gofile link storage service : Viewing API as user"
    });
  })
  .post(dataController.addGofileLink);

// Export API routes
module.exports = Router;
