const fs = require("fs");

// List all entries
exports.index = function (req, res) {
  try {
    // Read current file content
    const data = fs.readFileSync("./links.json", "utf8");
    const links = JSON.parse(data);

    // Answer with the list of available links
    res.json({
      status: "success",
      message: "The following links were uploaded to this server.",
      data: links
    });
  } catch (e) {
    // File not created yet: return []
    res.json({
      status: "success",
      message: "There is no data available on this server yet.",
      data: []
    });
  }
};

// Handle new submissions
exports.addGofileLink = function (req, res) {
  try {
    let links;

    // Attempt reading current content of file
    try {
      const data = fs.readFileSync("./links.json", "utf8");
      links = JSON.parse(data);
    } catch (e) {
      links = [];
    }

    // Create object from request parameters
    let linkToAdd = {
      timestamp: Date.now(),
      serviceID: req.body.serviceID,
      fileID: req.body.fileID,
      docName: req.body.docName
    };

    // Add object to previously available links
    links.push(linkToAdd);

    // Write new data to file
    const writeData = JSON.stringify(links, null, 4);
    fs.writeFileSync("./links.json", writeData, "utf8");

    // Answer with success!
    res.json({
      status: "success",
      message: "Data submitted successfully!",
      data: linkToAdd
    });
  } catch (e) {
    // An unknown error occured
    res.json({
      status: "error",
      message: "An error occured, please contact package maintainer."
    });
  }
};
