/* Open delete modal if there are any objects to delete */
function deleteSelectedObject() {
  event.preventDefault();
  if (doc.selectedObjectsNumber() > 0) {
    $("#delPopover").popover("show");
  }
}

$(function () {
  /* Handle color change */
  $(".color-tool").click(function () {
    $(".color-tool.active")[0].style.backgroundColor = "unset";
    $(".color-tool.active").removeClass("active");
    $(this).addClass("active");
    color = $(this).get(0).style.borderColor;
    $(this).get(0).style.backgroundColor = color;
    doc.setColor(color);
  });

  /* Handle brush size change */
  $("#brush-size").change(function () {
    var width = $(this).val();
    doc.setBrushSize(width);
  });

  /* Handle font size change */
  $("#font-size").change(function () {
    var font_size = $(this).val();
    doc.setFontSize(font_size);
  });

  $("#delPopover").blur(function () {
    $(this).popover("hide");
  });

  /* Handle tool change */
  $(document).on("cce:toolChanged", function () {
    $(".tool-button.active").removeClass("active");
    $(".tool-button.tool-code-" + doc.active_tool).addClass("active");
  });
});

/* Handle delete popover */
$(document).ready(function () {
  var popoverForm =
    '<button class="btn btn-danger btn-sm" onclick="doc.deleteSelectedObject()" type="button" aria-label="Close">Delete</button><button class="btn btn-light btn-sm" type="button" aria-label="Close">Cancel</button>';
  $("#delPopover").popover({
    title: "Are you sure?",
    content: popoverForm,
    placement: "bottom",
    trigger: "manual",
    html: true
  });
});
