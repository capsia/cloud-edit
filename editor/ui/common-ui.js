/*
 * Capsia Cloud Edit
 * by Riccardo Riccio
 * A simple, yet powerful document editor.
 * The aim of this editor is to support annotations and file sharing.
 * File sharing uses Gofile API and GetSimpleForm API
 */

// Global variables
var STORAGE_PROVIDER = {
  name: "GofileAPI",
  config: {}
};
var SHARE_PROVIDER = null;
var DOC_NAME = "Untitled document"; // Document name to show, save and send
var DOC_EXT = "pdf"; // Document type (pdf|odt)
var DOC_ID = null;
var doc;

/* Ask to exit if unsaved */
window.onbeforeunload = function () {
  return doc.isDocumentModified()
    ? "There are unsaved changes to the file."
    : null;
};

/* Load storage and share providers */
window.onload = function () {
  if (STORAGE_PROVIDER) {
    let resourceLoader = new AsyncJSLoader();
    resourceLoader.getLib(
      "./common/storageProviders/" + STORAGE_PROVIDER.name + ".js",
      function () {
        window.storageProvider = new window[STORAGE_PROVIDER.name](
          STORAGE_PROVIDER.config
        );
        if (SHARE_PROVIDER) {
          resourceLoader.getLib(
            "./common/shareProviders/" + SHARE_PROVIDER.name + ".js",
            function () {
              window.shareProvider = new window[SHARE_PROVIDER.name](
                SHARE_PROVIDER.config
              );
              loadDocument();
            }
          );
        } else {
          loadDocument();
        }
      }
    );
  } else {
    loadDocument();
  }
};

function loadDocument() {
  let cceURLObj = new URL(window.location.href),
    cceURL = cceURLObj.searchParams.get("cceurl");
  gfID = cceURLObj.searchParams.get("gfid");

  /* Check if public token is provided */
  if (!SHARE_PROVIDER || !STORAGE_PROVIDER) {
    let btnText = document.getElementById("SvSendBtnText");
    btnText.innerHTML = "Save";
  }

  /* Detect method to start editor */

  // Check for saved versions
  DOC_ID = cceURL ? cceURL : gfID ? gfID : "doc.pdf";
  let latestVersionTarget = window.localStorage.getItem(DOC_ID + "_latest");

  // A) Start from previous saved version
  if (latestVersionTarget) {
    startFromGofile(JSON.parse(latestVersionTarget).gfID);

    // B) From Gofile ID
  } else if (gfID) {
    startFromGofile(gfID);

    // C) From given URL
  } else if (cceURL) {
    startFromUrl(cceURL);

    // D) Use default file
  } else {
    initCCE("doc.pdf");
  }
}

function startFromUrl(cceURL) {
  // Get document details from URL
  DOC_NAME = cceURL.split("\\").pop().split("/").pop().split(".").shift();
  DOC_EXT = cceURL.split("\\").pop().split("/").pop().split(".").pop();

  // Start Cloud Edit from given url
  initCCE(cceURL);
}

function startFromGofile(gfID) {
  // Get direct URL
  window.storageProvider.getFilesLink(gfID, function (r) {
    let GFdoc = r.files[Object.keys(r.files)[0]]; //  Direct URL

    // Get document details from upload name
    DOC_NAME = GFdoc.name.split(".").shift();
    DOC_EXT = GFdoc.name.split(".").pop();
    DOC_ID = gfID;

    // Start Cloud Edit from given url
    initCCE(GFdoc.link);
  });
}

// Initialize editor from given URL
function initCCE(docURL) {
  let resourceLoader = new AsyncJSLoader();

  if (DOC_EXT == "pdf") {
    resourceLoader.getLib(
      [
        "https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.328/pdf.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/fabric.js/4.1.0/fabric.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js",
        "./pdf/arrow.fabric.js?v=1",
        "./pdf/pdf-api.js?v=3",
        "./ui/pdf-ui.js?v=4"
      ],
      function () {
        // Disable tools for odt only
        $(".odt-tool").each(function (i, obj) {
          obj.style.display = "none";
        });
        doc = new PDFAnnotate("editorContainer", docURL, {
          ready() {
            // Stop loading animation
            document.getElementById("cceSpinner").style.display = "none";
            document.getElementById("docName").innerText = DOC_NAME;
          },

          onPageUpdated(page, oldData, newData) {
            // Set unsaved state
            doc.setDocumentModified(true);
          }
        });
      }
    );
  } else if (DOC_EXT == "odt") {
    resourceLoader.getLib(
      ["./odt/wodotexteditor.js", "./ui/odt-ui.js?v=3"],
      function () {
        // Disable tools for pdf only
        $(".pdf-tool").each(function (i, obj) {
          obj.style.display = "none";
        });
        doc = new ODTReview("editorContainer", docURL, {
          ready() {
            // Stop loading animation
            document.getElementById("cceSpinner").style.display = "none";
            document.getElementById("docName").innerText = DOC_NAME;
          }
        });
      }
    );
  } else {
    // Document format not recognised! CCE won't load.
    alert("Error 01: Unrecognised document format!");
  }
}

// Upload success event listener
document.addEventListener("gf-api:upload-success", function (e, r) {
  e.preventDefault();
  req = JSON.parse(e.detail);
  window.localStorage.setItem(
    DOC_ID + "_latest",
    JSON.stringify({
      timestamp: Date.now(),
      gfID: req.data.code
    })
  );
  window.shareProvider.sendEntry({
    serviceID: "capsiaCloudEdit",
    fileID: req.data.code,
    docName: DOC_NAME
  });
});

function saveInteraction() {
  doc.getDoc(saveDoc);
}

function sentSuccess(docBlob) {
  // Show sent status in UI
  $("#saveSendBtn")[0].style.backgroundColor = "#73d216";
  $("#SvSendBtnText")[0].innerText = " Success!";
  $(".fa-circle-o-notch")[0].className = "fa fa-save";

  // Set as unmodified and download
  doc.setDocumentModified(false);
  saveAs(docBlob, DOC_NAME + "." + DOC_EXT);

  setTimeout(function () {
    // Show default status in UI
    $("#saveSendBtn")[0].style.backgroundColor = "";
    $("#saveSendBtn")[0].disabled = false;
    $("#SvSendBtnText")[0].innerText = " Save and send";
  }, 5000);
}

function sentFailure(docBlob) {
  // Show sent status in UI
  $("#saveSendBtn")[0].style.backgroundColor = "#d9534f";
  $("#SvSendBtnText")[0].innerText = " Error!";
  $(".fa-circle-o-notch")[0].className = "fa fa-times";

  // Download
  saveAs(docBlob, DOC_NAME + "." + DOC_EXT);

  setTimeout(function () {
    // Show default status in UI
    $("#saveSendBtn")[0].style.backgroundColor = "";
    $("#saveSendBtn")[0].disabled = false;
    $("#SvSendBtnText")[0].innerText = " Save and send";
    $(".fa-times")[0].className = "fa fa-save";
  }, 5000);
}

function saveDoc(docBlob) {
  // Download pdf and send to owner
  if (SHARE_PROVIDER && STORAGE_PROVIDER) {
    // Listen for uploaded events
    document.addEventListener("gsf-api:send-success", function (e) {
      e.preventDefault();
      sentSuccess(docBlob);
      document.removeEventListener("gsf-api:send-success", arguments.callee);
    });

    document.addEventListener("gsf-api:request-failed", function (e) {
      e.preventDefault();
      sentFailure(docBlob);
      document.removeEventListener("gsf-api:request-failed", arguments.callee);
    });

    document.addEventListener("gf-api:request-failed", function (e) {
      e.preventDefault();
      sentFailure(docBlob);
      document.removeEventListener("gf-api:request-failed", arguments.callee);
    });

    // Set sending state to UI
    $("#saveSendBtn")[0].style.backgroundColor = "#85ffee";
    $("#saveSendBtn")[0].disabled = true;
    $("#SvSendBtnText")[0].innerText = " Sending...";
    $(".fa-save")[0].className = "fa fa-circle-o-notch fa-pulse";

    // Init upload
    window.storageProvider.sendFile(docBlob, DOC_NAME + "." + DOC_EXT);
  } else {
    // Download doc
    saveAs(docBlob, DOC_NAME + "." + DOC_EXT);
    doc.setDocumentModified(false);
  }
}

/* Open rename dialog */
function openRenameDialog() {
  $("#docRenameDialog")[0].style.display = "inline";
  $("#docName")[0].style.display = "none";
  $("#docRename")[0].value = DOC_NAME;
}

/* Dismiss rename dialog */
function dismissRename() {
  $("#docRenameDialog")[0].style.display = "";
  $("#docName")[0].style.display = "";
}

/* Rename document */
function saveRename() {
  let userInput = document.getElementById("docRename");
  DOC_NAME = userInput.value ? userInput.value : DOC_NAME;
  document.getElementById("docName").innerText = DOC_NAME;
  dismissRename();
}
