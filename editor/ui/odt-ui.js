/**
 * Copyright (C) 2014 KO GmbH <copyright@kogmbh.com>
 *
 * @licstart
 * This file is part of WebODF.
 *
 * WebODF is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License (GNU AGPL)
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * WebODF is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with WebODF.  If not, see <http://www.gnu.org/licenses/>.
 * @licend
 *
 * @source: http://www.webodf.org/
 * @source: https://github.com/kogmbh/WebODF/
 */

/*global document, window, runtime, FileReader, alert, Uint8Array, Blob, saveAs, Wodo*/

var ODTReview = function (container_id, url, options = {}) {
  this.editor = null;
  this.editedFlag = false;
  var inst = this;

  this.getDoc = function (callback) {
    inst.editor.getDocumentAsByteArray(function (err, data) {
      if (!err) {
        callback(
          new Blob([data.buffer], {
            type: "application/vnd.oasis.opendocument.text"
          })
        );
      }
    });
  };

  this.onEditorCreated = function (err, e) {
    if (err) {
      // something failed unexpectedly
      alert(err);
      return;
    }

    inst.editor = e;
    inst.editor.setUserData({
      fullName: "Reviewer",
      color: "red"
    });

    inst.editor.openDocumentFromUrl(url, options.ready);
  };

  this.isDocumentModified = function () {
    return inst.editor.isDocumentModified() || inst.editedFlag;
  };

  this.setDocumentModified = function (value) {
    inst.editor.setDocumentModified(value) || true;
    inst.editedFlag = value;
  };

  this.addAnnotation = function () {
    inst.setDocumentModified(true);
    $("#dijit_form_Button_1")[0].parentNode.click();
  };

  let cceURLObj = new URL(window.location.href),
    editorMode = cceURLObj.searchParams.get("mode");

  Wodo.createTextEditor(
    "editorContainer",
    {
      modus: editorMode == "edit" ? Wodo.MODUS_FULLEDITING : Wodo.MODUS_REVIEW
    },
    inst.onEditorCreated
  );
};
