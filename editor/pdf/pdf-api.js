/**
 * PDFAnnotate v1.0.0
 * Author: Ravisha Heshan
 */

var PDFAnnotate = function (container_id, url, options = {}) {
  this.number_of_pages = 0;
  this.pages_rendered = 0;
  this.active_tool = 1; // 1 - Free hand, 2 - Text, 3 - Arrow, 4 - Rectangle
  this.fabricObjects = [];
  this.fabricObjectsData = [];
  this.color = "#212121";
  this.borderColor = "#000000";
  this.borderSize = 1;
  this.font_size = 16;
  this.active_canvas = 0;
  this.container_id = container_id;
  this.url = url;
  this.editedFlag = false;
  var inst = this;

  var loadingTask = PDFJS.getDocument(this.url);
  loadingTask.promise.then(
    function (pdf) {
      var scale = 1.3;
      inst.number_of_pages = pdf.pdfInfo.numPages;

      for (var i = 1; i <= pdf.pdfInfo.numPages; i++) {
        pdf.getPage(i).then(function (page) {
          var viewport = page.getViewport(scale);
          var canvas = document.createElement("canvas");
          document.getElementById(inst.container_id).appendChild(canvas);
          canvas.className = "pdf-canvas";
          canvas.height = viewport.height;
          canvas.width = viewport.width;
          context = canvas.getContext("2d");

          var renderContext = {
            canvasContext: context,
            viewport: viewport
          };
          var renderTask = page.render(renderContext);
          renderTask.then(function () {
            $(".pdf-canvas").each(function (index, el) {
              $(el).attr("id", "page-" + (index + 1) + "-canvas");
            });
            inst.pages_rendered++;
            if (inst.pages_rendered == inst.number_of_pages) inst.initFabric();
          });
        });
      }
    },
    function (reason) {
      console.error(reason);
    }
  );

  this.initFabric = function () {
    var inst = this;
    let canvases = $("#" + inst.container_id + " canvas");
    canvases.each(function (index, el) {
      var background = el.toDataURL("image/png");
      var fabricObj = new fabric.Canvas(el.id, {
        freeDrawingBrush: {
          width: 1,
          color: inst.color
        }
      });
      inst.fabricObjects.push(fabricObj);
      if (typeof options.onPageUpdated == "function") {
        fabricObj.on("object:added", function () {
          var oldValue = Object.assign({}, inst.fabricObjectsData[index]);
          inst.fabricObjectsData[index] = fabricObj.toJSON();
          options.onPageUpdated(
            index + 1,
            oldValue,
            inst.fabricObjectsData[index]
          );
        });
      }
      fabricObj.setBackgroundImage(
        background,
        fabricObj.renderAll.bind(fabricObj)
      );
      $(fabricObj.upperCanvasEl).click(function (event) {
        inst.active_canvas = index;
        inst.fabricClickHandler(event, fabricObj);
      });
      fabricObj.on("after:render", function () {
        inst.fabricObjectsData[index] = fabricObj.toJSON();
        fabricObj.off("after:render");
      });

      if (
        index === canvases.length - 1 &&
        typeof options.ready === "function"
      ) {
        options.ready();
      }
    });
  };

  this.fabricClickHandler = function (event, fabricObj) {
    var inst = this;

    var editInstead = function () {
      var selectedObjects = inst.fabricObjects[
        inst.active_canvas
      ].getActiveObjects();
      if (selectedObjects.length > 1) {
        inst.active_tool = 0;
        $(document).trigger("cce:toolChanged");
      } else if (
        selectedObjects.length == 1 &&
        selectedObjects[0].type == "i-text"
      ) {
        inst.active_tool = 0;
        $(document).trigger("cce:toolChanged");
      }
    };

    editInstead();

    if (inst.active_tool == 2) {
      var text = new fabric.IText("", {
        left:
          event.clientX - fabricObj.upperCanvasEl.getBoundingClientRect().left,
        top:
          event.clientY - fabricObj.upperCanvasEl.getBoundingClientRect().top,
        fill: inst.color,
        fontSize: inst.font_size,
        selectable: true
      });
      fabricObj.add(text);
      text.enterEditing();
      text.on("editing:exited", function () {
        if (text.text == "") {
          inst.fabricObjects[inst.active_canvas].remove(text);
        }
      });
      inst.fabricObjects[inst.active_canvas].on(
        "selection:created",
        function () {
          text.exitEditing();
          editInstead();
        }
      );
      inst.fabricObjects[inst.active_canvas].on(
        "selection:updated",
        function () {
          text.exitEditing();
          editInstead();
        }
      );
    }
  };
};

/* Enable selector tool ( ToolID: 0 ) */
PDFAnnotate.prototype.enableSelector = function () {
  var inst = this;
  inst.active_tool = 0;
  $(document).trigger("cce:toolChanged");
  if (inst.fabricObjects.length > 0) {
    $.each(inst.fabricObjects, function (index, fabricObj) {
      fabricObj.isDrawingMode = false;
    });
  }
};

/* Enable pencil tool ( ToolID: 1 ) */
PDFAnnotate.prototype.enablePencil = function () {
  var inst = this;
  inst.active_tool = 1;
  $(document).trigger("cce:toolChanged");
  if (inst.fabricObjects.length > 0) {
    $.each(inst.fabricObjects, function (index, fabricObj) {
      fabricObj.isDrawingMode = true;
    });
  }
};

/* Enable text tool ( ToolID: 2 ) */
PDFAnnotate.prototype.enableAddText = function () {
  var inst = this;
  inst.active_tool = 2;
  $(document).trigger("cce:toolChanged");
  if (inst.fabricObjects.length > 0) {
    $.each(inst.fabricObjects, function (index, fabricObj) {
      fabricObj.isDrawingMode = false;
    });
  }
};

/* Enable rectangle tool ( ToolID: 4 ) */
PDFAnnotate.prototype.enableRectangle = function () {
  var inst = this;
  var fabricObj = inst.fabricObjects[inst.active_canvas];
  inst.active_tool = 4;
  $(document).trigger("cce:toolChanged");
  if (inst.fabricObjects.length > 0) {
    $.each(inst.fabricObjects, function (index, fabricObj) {
      fabricObj.isDrawingMode = false;
    });
  }

  inst.setColor("rgba(255, 0, 0, 0.3)");
  inst.setBorderColor("blue");

  var rect = new fabric.Rect({
    width: 100,
    height: 100,
    fill: inst.color,
    stroke: inst.borderColor,
    strokeSize: inst.borderSize
  });
  fabricObj.add(rect);
};

/* Enable arrow tool ( ToolID: 3 ) */
PDFAnnotate.prototype.enableAddArrow = function () {
  var inst = this;
  inst.active_tool = 3;
  $(document).trigger("cce:toolChanged");
  if (inst.fabricObjects.length > 0) {
    $.each(inst.fabricObjects, function (index, fabricObj) {
      fabricObj.isDrawingMode = false;
      new Arrow(fabricObj, inst.color, function () {
        inst.active_tool = 0;
        $(document).trigger("cce:toolChanged");
      });
    });
  }
};

/* Get number of selected objects */
PDFAnnotate.prototype.selectedObjectsNumber = function () {
  return this.fabricObjects[this.active_canvas].getActiveObjects().length;
};

/* Delete selected objects */
PDFAnnotate.prototype.deleteSelectedObject = function () {
  var inst = this;
  var activeObjects = inst.fabricObjects[inst.active_canvas].getActiveObjects();
  if (activeObjects.length > 0) {
    for (let el of activeObjects)
      inst.fabricObjects[inst.active_canvas].remove(el);
  }
};

/* Elaborate pdf */
PDFAnnotate.prototype.getDoc = function (callback) {
  var inst = this;
  var doc = new jsPDF();
  $.each(inst.fabricObjects, function (index, fabricObj) {
    if (index != 0) {
      doc.addPage();
      doc.setPage(index + 1);
    }
    doc.addImage(fabricObj.toDataURL(), "png", 0, 0);
  });
  callback(doc.output("blob"));
};

/* Set pencil size */
PDFAnnotate.prototype.setBrushSize = function (size) {
  var inst = this;
  $.each(inst.fabricObjects, function (index, fabricObj) {
    fabricObj.freeDrawingBrush.width = size;
  });
};

/* Set color for drawing */
PDFAnnotate.prototype.setColor = function (color) {
  var inst = this;
  inst.color = color;
  $.each(inst.fabricObjects, function (index, fabricObj) {
    fabricObj.freeDrawingBrush.color = color;
  });
};

/* Set border color for rectangles */
PDFAnnotate.prototype.setBorderColor = function (color) {
  this.borderColor = color;
};

/* Set text dimension */
PDFAnnotate.prototype.setFontSize = function (size) {
  this.font_size = size;
};

/* Set border size for rectangles */
PDFAnnotate.prototype.setBorderSize = function (size) {
  this.borderSize = size;
};

/* Delete all the drawings on the current page */
PDFAnnotate.prototype.clearActivePage = function () {
  var inst = this;
  var fabricObj = inst.fabricObjects[inst.active_canvas];
  var bg = fabricObj.backgroundImage;
  if (confirm("Are you sure?")) {
    fabricObj.clear();
    fabricObj.setBackgroundImage(bg, fabricObj.renderAll.bind(fabricObj));
  }
};

/* Serialize all drawings to JSON */
PDFAnnotate.prototype.serializePdf = function () {
  var inst = this;
  return JSON.stringify(inst.fabricObjects, null, 4);
};

/* Load annotations from a previous save in JSON format */
PDFAnnotate.prototype.loadFromJSON = function (jsonData) {
  var inst = this;
  $.each(inst.fabricObjects, function (index, fabricObj) {
    if (jsonData.length > index) {
      fabricObj.loadFromJSON(jsonData[index], function () {
        inst.fabricObjectsData[index] = fabricObj.toJSON();
      });
    }
  });
};

/* Get edited flag */
PDFAnnotate.prototype.isDocumentModified = function () {
  var inst = this;
  return inst.editedFlag;
};

/* Get edited flag */
PDFAnnotate.prototype.setDocumentModified = function (value) {
  var inst = this;
  inst.editedFlag = value;
};
