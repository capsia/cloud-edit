var GofileAPI = function (options = {}) {
  var inst = this;

  /*
   * Send files to Gofile API
   * Parameters: fileAsBlob
   */
  this.sendFile = function (fileAsBlob, fileName) {
    inst.makeRequest("https://apiv2.gofile.io/getServer", function (r) {
      let req = JSON.parse(r),
        serverID = req.data.server,
        convertToFile = new File([fileAsBlob], fileName, {
          type: "text/plain"
        }),
        formData = new FormData();
      formData.append("file", convertToFile);

      inst.makeRequest(
        "https://" + serverID + ".gofile.io/uploadFile",
        function (r) {
          let event = new CustomEvent("gf-api:upload-success", { detail: r });
          document.dispatchEvent(event);
        },
        formData,
        "POST"
      );
    });
  };

  /*
   * Check if a file exists on Gofile server
   * Parameters: uploadID, callback
   */
  this.existsFile = function (uploadID, callback) {
    inst.makeRequest(
      "https://apiv2.gofile.io/getServer?c=" + uploadID,
      function (r) {
        let req = JSON.parse(r);
        callback(req.status == "ok");
      }
    );
  };

  /*
   * Get file list from Gofile API
   * Parameters: uploadID, callback
   */
  this.getFilesLink = function (uploadID, callback) {
    inst.makeRequest(
      "https://apiv2.gofile.io/getServer?c=" + uploadID,
      function (r) {
        let req = JSON.parse(r);
        inst.makeRequest(
          "https://" + req.data.server + ".gofile.io/getUpload?c=" + uploadID,
          function (r) {
            let req = JSON.parse(r);
            if (req.status == "ok") {
              callback(req.data);
            } else {
              inst.logFailed("File is unavailable!");
            }
          }
        );
      }
    );
  };

  this.makeRequest = function (
    url,
    callback,
    reqContent = "",
    reqType = "GET"
  ) {
    let http = new XMLHttpRequest();
    http.open(reqType, url, true);

    http.onload = function () {
      if (http.status >= 200 && http.status < 400) {
        callback(http.response);
      } else {
        inst.logFailed("Server response: " + http.status);
      }
    };

    http.onerror = function (e) {
      inst.logFailed(e);
    };

    http.send(reqContent);
  };

  this.logFailed = function (e) {
    let event = new CustomEvent("gf-api:request-failed", { detail: e });
    document.dispatchEvent(event);
  };
};
