var AsyncJSLoader = function () {
  var inst = this;

  /*
   * Load JS library
   * Parameters: filePath, callback function
   */
  this.getLib = function (filePath, callback) {
    let currentPath;
    if (filePath instanceof Array) {
      currentPath = filePath.shift();
    } else {
      currentPath = filePath;
    }

    if (currentPath) {
      let script = document.createElement("script");
      script.src = currentPath;
      script.type = "text/javascript";

      if (!(filePath instanceof Array) || filePath.length == 0) {
        script.onload = callback;
      } else {
        script.onload = function () {
          inst.getLib(filePath, callback);
        };
      }

      document.getElementsByTagName("head")[0].appendChild(script);
    } else {
      console.log("Nothing to do!");
    }
  };
};
