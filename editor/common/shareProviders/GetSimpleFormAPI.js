/* GetSimpleForm API */

var GetSimpleFormAPI = function (options = {}) {
  this.publicToken = options.token;
  this.adminToken = options.adminToken;
  var inst = this;

  /*
   * Send data to getSimpleForm API
   * Needs a publicToken
   * Parameters: content ( a dictionary of all form values )
   */
  this.sendEntry = function (content) {
    /* Extract data */
    let urlEncodedData = "",
      urlEncodedDataPairs = ["redirect_to=https://getsimpleform.com/"];

    for (let name in content) {
      urlEncodedDataPairs.push(
        encodeURIComponent(name) + "=" + encodeURIComponent(content[name])
      );
    }
    urlEncodedData = urlEncodedDataPairs.join("&").replace(/%20/g, "+");

    inst.makeRequest(
      "https://getsimpleform.com/messages?form_api_token=" + inst.publicToken,
      function (r) {
        let event = new CustomEvent("gsf-api:send-success", { detail: r });
        document.dispatchEvent(event);
      },
      urlEncodedData,
      "POST"
    );
  };

  /*
   * Get all entries in a getSimpleForm account
   * Needs an adminToken
   * Parameters: callback
   * TODO: This function needs testing (check if Content-type header is okay)
   */
  this.getEntries = function (callback) {
    inst.makeRequest(
      "https://getsimpleform.com/messages.json?api_token=" + inst.adminToken,
      function (r) {
        let req = JSON.parse(r);
        callback(req);
      }
    );
  };

  /*
   * Delete an entry from getSimpleForm
   * Needs an adminToken
   * Parameters: entryID
   * TODO: This function needs testing
   */
  this.deleteEntry = function (entryID) {
    let urlEncodedData = "_method=delete";
    inst.makeRequest(
      "https://getsimpleform.com/messages/" +
        entryID +
        "?api_token=" +
        inst.adminToken,
      function (r) {
        let event = new CustomEvent("gsf-api:delete-success", { detail: r });
        document.dispatchEvent(event);
      },
      urlEncodedData,
      "POST"
    );
  };

  this.makeRequest = function (
    url,
    callback,
    reqContent = "",
    reqType = "GET"
  ) {
    let http = new XMLHttpRequest();
    http.open(reqType, url, true);

    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    http.onload = function () {
      if (http.status >= 200 && http.status < 400) {
        callback(http.response);
      } else {
        inst.logFailed("Server response: " + http.status);
      }
    };

    http.onerror = function (e) {
      inst.logFailed(e);
    };

    http.send(reqContent);
  };

  this.logFailed = function (e) {
    let event = new CustomEvent("gsf-api:request-failed", { detail: e });
    document.dispatchEvent(event);
  };
};
