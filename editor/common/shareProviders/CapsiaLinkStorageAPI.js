/* CapsiaLinkStorage API */

var CapsiaLinkStorageAPI = function (options = {}) {
  this.url = options.url;
  var inst = this;

  /*
   * Send data to CapsiaLinkStorage API
   * Needs an url
   * Parameters: content ( a dictionary of all form values )
   */
  this.sendEntry = function (content) {
    /* Extract data */
    let urlEncodedData = "",
      urlEncodedDataPairs = [];

    for (let name in content) {
      urlEncodedDataPairs.push(
        encodeURIComponent(name) + "=" + encodeURIComponent(content[name])
      );
    }
    urlEncodedData = urlEncodedDataPairs.join("&").replace(/%20/g, "+");

    inst.makeRequest(
      inst.url,
      function (r) {
        let event = new CustomEvent("gsf-api:send-success", { detail: r });
        document.dispatchEvent(event);
      },
      urlEncodedData,
      "POST"
    );
  };

  this.makeRequest = function (
    url,
    callback,
    reqContent = "",
    reqType = "GET"
  ) {
    let http = new XMLHttpRequest();
    http.open(reqType, url, true);

    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    http.onload = function () {
      if (http.status >= 200 && http.status < 400) {
        callback(http.response);
      } else {
        inst.logFailed("Server response: " + http.status);
      }
    };

    http.onerror = function (e) {
      inst.logFailed(e);
    };

    http.send(reqContent);
  };

  this.logFailed = function (e) {
    let event = new CustomEvent("gsf-api:request-failed", { detail: e });
    document.dispatchEvent(event);
  };
};
