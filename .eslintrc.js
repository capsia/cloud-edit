module.exports = {
  extends: ["plugin:prettier/recommended", "prettier"],
  parserOptions: { ecmaVersion: 6 },
  rules: {
    "prettier/prettier": "error"
  }
};
